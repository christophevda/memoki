/* deck_row.vala
 *
 * Copyright 2022 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    [GtkTemplate (ui = "/com/protonmail/christophe/vda/Memoki/deck_row.ui")]
    public class DeckRow : Adw.ActionRow {
        [GtkChild]
        unowned Gtk.Popover delete_deck_popover;

        public bool edit_mode_active { get; set; }

        public ShortcutDeck deck;
        public Model model;

        public DeckRow (ShortcutDeck d, Model m) {
            Object ();

            deck = d;
            model = m;
            update_shortcuts_info ();

            deck.bind_property(
                "name",
                this,
                "title",
                BindingFlags.SYNC_CREATE
            );

            deck.shortcuts.notify.connect (update_shortcuts_info);
        }

        private void update_shortcuts_info () {
            this.subtitle = "%g shortcuts.".printf (deck.shortcuts.n_items);
        }

        [GtkCallback]
        private void on_cancel_button_clicked () {
            delete_deck_popover.popdown ();
        }

        [GtkCallback]
        private void on_delete_button_clicked () {
            model.remove_deck (deck);
        }
    }
}
