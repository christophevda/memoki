/* edit_view.vala
 *
 * Copyright 2022 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    [GtkTemplate (ui = "/com/protonmail/christophe/vda/Memoki/shortcut_row.ui")]
    public class ShortcutRow : Gtk.ListBoxRow {
        [GtkChild]
        private unowned Gtk.Button record_button;
        [GtkChild]
        private unowned Gtk.Box shortcut_label_box;
        [GtkChild]
        private unowned Gtk.Entry description_entry;
        [GtkChild]
        private unowned Gtk.ScrolledWindow accelerators_widget;
        [GtkChild]
        private unowned Gtk.Stack description_stack;
        [GtkChild]
        private unowned Gtk.Label description_label;
        [GtkChild]
        private unowned Gtk.Revealer record_button_revealer;

        private Shortcut shortcut;
        public Model model;

        public bool edit_mode_active { get; set; }
        public string[] accelerators { get; set; }

        private static Gtk.SizeGroup description_sg = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);

        public signal void entry_activate (int row_index);

        public ShortcutRow (Shortcut s, Model m) {
            Object ();

            notify["edit-mode-active"].connect(update_mode);
            notify["accelerators"].connect(update_accelerators_widget);

            shortcut = s;
            model = m;
            description_sg.add_widget (description_stack);
            
            shortcut.bind_property(
                "description",
                description_label,
                "label",
                BindingFlags.SYNC_CREATE
            );

            shortcut.bind_property(
                "description",
                description_entry.buffer,
                "text",
                BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL
            );

            shortcut.bind_property(
                "accelerators",
                this,
                "accelerators",
                BindingFlags.SYNC_CREATE
            );
        }

        public void enter_focus () {
            record_button.grab_focus ();
        }

        private void update_mode () {
            description_stack.set_visible_child_name(
                edit_mode_active ? "description_entry" : "description_label"
            );
        }

        private void update_accelerators_widget () {
            remove_children(shortcut_label_box);

            if (_accelerators.length == 0) {
                record_button.set_label (_("New Shortcut"));
                accelerators_widget.set_visible (false);
                return;
            }

            record_button.set_icon_name ("document-edit-symbolic");
            accelerators_widget.set_visible (true);
            foreach (string a in _accelerators) {
                shortcut_label_box.append (new Gtk.ShortcutLabel (a));
            }

            description_entry.grab_focus ();
        }

        [GtkCallback]
        private void on_record_button_clicked () {
            var record_window = new RecordWindow (shortcut);
            record_window.set_modal (true);
            record_window.set_transient_for (get_window (this));
            record_window.show ();
        }

        [GtkCallback]
        private void on_delete_button_clicked () {
            if (shortcut.is_empty ()) return;

            model.remove_shortcut (shortcut);
        }

        [GtkCallback]
        private void on_description_entry_activate () {
            entry_activate (get_index ());
        }
    }
}
