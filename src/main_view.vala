/* main_view.vala
 *
 * Copyright 2022 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    [GtkTemplate (ui = "/com/protonmail/christophe/vda/Memoki/main_view.ui")]
    public class MainView : Adw.NavigationPage {
        [GtkChild]
        private unowned Gtk.ListBox deck_list_box;
        [GtkChild]
        private unowned Gtk.Entry add_deck_entry;
        [GtkChild]
        private unowned Gtk.Popover add_deck_popover;

        public Model model;

        public MainView (Model m) {
            Object ();

            model = m;

            deck_list_box.bind_model (model.data.decks, create_deck_row);
        }

        private Gtk.Widget create_deck_row (Object o) {
            var deck = o as ShortcutDeck;
            var row = new DeckRow (deck, model);
            return row;
        }

        [GtkCallback]
        private void on_row_activated (Gtk.ListBoxRow r) {
            var row = r as DeckRow;
            model.selected_deck = row.deck;
        }

        [GtkCallback]
        private void on_add_deck_button_clicked () {
            add_deck_popover.popdown ();

            model.add_deck (add_deck_entry.buffer.text);
        }
    }
}
