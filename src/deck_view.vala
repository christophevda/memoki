/* deck_view.vala
 *
 * Copyright 2022 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    [GtkTemplate (ui = "/com/protonmail/christophe/vda/Memoki/deck_view.ui")]
    public class DeckView : Adw.NavigationPage {
        [GtkChild]
        private unowned Adw.WindowTitle window_title;
        [GtkChild]
        private unowned Gtk.Entry window_title_entry;
        [GtkChild]
        private unowned Gtk.ListBox shortcut_list_box;
        [GtkChild]
        private unowned Gtk.Stack title_stack;
        [GtkChild]
        private unowned Gtk.ToggleButton edit_toggle;

        public bool edit_mode_active { get; set; }

        private Model model;

        private BindingGroup deck_bindings;

        public DeckView (Model m) {
            Object ();

            model = m;
            deck_bindings = new BindingGroup ();
            setup_deck_bindings ();

            model.notify["selected-deck"].connect (update_deck);
            edit_toggle.notify["active"].connect (on_edit_active);

            edit_toggle.bind_property (
                                       "active",
                                       this,
                                       "edit-mode-active",
                                       BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL
            );

            bind_property (
                           "edit_mode_active",
                           model,
                           "deck_edit_mode_active",
                           BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL
            );
        }

        private void setup_deck_bindings () {
            deck_bindings.bind_property (
                                         "name",
                                         window_title,
                                         "title",
                                         BindingFlags.SYNC_CREATE
            );

            deck_bindings.bind_property (
                                         "name",
                                         window_title_entry.buffer,
                                         "text",
                                         BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL
            );
        }

        private void update_deck () {
            var deck = model.selected_deck;

            if (deck == null) return;

            shortcut_list_box.bind_model (
                                          deck.shortcuts, create_shortcut_row);

            deck_bindings.set_source (deck);

            if (deck.shortcuts.n_items == 0) edit_mode_active = true;
        }

        private Gtk.Widget create_shortcut_row (Object o) {
            var shortcut = o as Shortcut;
            var row = new ShortcutRow (shortcut, model);
            bind_property (
                           "edit_mode_active",
                           row,
                           "edit-mode-active",
                           BindingFlags.SYNC_CREATE
            );
            row.entry_activate.connect (on_row_entry_activate);
            return row;
        }

        private void on_row_entry_activate (int index) {
            var next_row = shortcut_list_box.get_row_at_index (index + 1)
                as ShortcutRow;

            if (next_row != null) {
                next_row.enter_focus ();
            }
        }

        private void on_edit_active () {
            var visible_child_name = edit_toggle.active ? "entry" : "label";
            title_stack.visible_child_name = visible_child_name;
        }

        [GtkCallback]
        private void on_edit_banner_button_clicked () {
            edit_mode_active = false;
        }
    }
}
