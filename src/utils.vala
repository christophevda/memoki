/* utils.vala
 *
 * Copyright 2022 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    const string memoki_data_dir = "Memoki";
    const string memoki_data_file = "data.json";

    void remove_children (Gtk.Box box) {
        var child = box.get_last_child ();
        while (child != null) {
            box.remove (child);
            child = box.get_last_child ();
        }
    }

    Gtk.Window get_window (Gtk.Widget widget) {
        return (Gtk.Window) widget.get_ancestor (typeof (Gtk.Window));
    }

    string get_data_filename () {
        var base_dir = Environment.get_user_data_dir ();
        return Path.build_filename (base_dir, memoki_data_dir, memoki_data_file);
    }

    ShortcutDeckCollection load_deck_data () {
        create_data_location_if_not_exists ();

        Json.Parser parser = new Json.Parser ();

        try {
            parser.load_from_file (get_data_filename ());
            Json.Node root = parser.get_root ();

            ShortcutDeckCollection data = Json.gobject_deserialize (
                typeof (ShortcutDeckCollection),
                root
            ) as ShortcutDeckCollection;

            assert (data != null);

            return data;
        } catch (Error e) {
            print ("Unable to parse `%s': %s\n", get_data_filename (), e.message);
            return new ShortcutDeckCollection ();
        }
    }

    void save_deck_data (ShortcutDeckCollection data) {
        Json.Node root = Json.gobject_serialize (data);

        var generator = new Json.Generator ();
        generator.set_root (root);
        generator.to_file (get_data_filename ());
    }

    void create_data_location_if_not_exists () {
        var base_dir = Environment.get_user_data_dir ();
        var data_path = Path.build_filename (base_dir, memoki_data_dir);
        var file = File.new_for_path (data_path);

        try {
		    file.make_directory_with_parents ();
	    } catch (Error e) {
		    print ("Error: %s\n", e.message);
	    }
    }
}
