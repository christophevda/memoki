namespace Memoki {
    [GtkTemplate (ui = "/com/protonmail/christophe/vda/Memoki/record_window.ui")]
    public class RecordWindow : Adw.MessageDialog {
        [GtkChild]
        private unowned Gtk.Box shortcut_box;

        private string[] accelerators;
        private Shortcut shortcut;

        public RecordWindow (Shortcut shortcut) {
            Object ();

            this.shortcut = shortcut;
            setup_event_controller ();
            setup_responses ();
        }

        private void setup_event_controller () {
            var event_controller = new Gtk.EventControllerKey ();
            event_controller.key_pressed.connect (on_key_pressed);

            ((Gtk.Widget) this).add_controller (event_controller);
        }

        private void setup_responses () {
            this.add_response ("cancel", _("Cancel"));
            this.add_response ("confirm", _("Confirm"));
            this.set_response_appearance("confirm", Adw.ResponseAppearance.SUGGESTED);
            this.set_default_response("confirm");

            this.response.connect (on_response);
        }

        private bool on_key_pressed (
            Gtk.EventControllerKey event_controller,
            uint key,
            uint _,
            Gdk.ModifierType modifier) {

            var event = (Gdk.KeyEvent) event_controller.get_current_event ();
            var keyval = event.get_keyval ();

            if (keyval == Gdk.Key.Escape ||
                keyval == Gdk.Key.Return ||
                keyval == Gdk.Key.KP_Enter) {
                return false;
            }
            else if (!event.is_modifier ()) {
                if (this.accelerators.length == 0) {
                    remove_children(this.shortcut_box);
                }

                var accelerator = Gtk.accelerator_name (key, modifier);
                var shortcut_label = new Gtk.ShortcutLabel (accelerator);

                this.accelerators += accelerator;
                this.shortcut_box.append (shortcut_label);

                message (accelerator);
            }

            return true;
        }

        private void on_response (
            Adw.MessageDialog dialog,
            string response) {

            if (response == "confirm") {
                this.shortcut.accelerators = this.accelerators;
            }
        }
    }
}

