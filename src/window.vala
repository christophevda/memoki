/* window.vala
 *
 * Copyright 2023 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    [GtkTemplate (ui = "/com/protonmail/christophe/vda/Memoki/window.ui")]
    public class Window : Adw.ApplicationWindow {
        [GtkChild]
        private unowned Adw.NavigationView navigation_view;

        private Model model;

        public Window (Gtk.Application app) {
            Object (application: app);

            model = new Model (load_deck_data ());

            model.notify["selected-deck"].connect (update_stack);
            connect ("signal::destroy", on_destroy, null);
            navigation_view.popped.connect (on_popped);

            setup_views ();
        }

        private void setup_views () {
            navigation_view.add (new MainView (model));
            navigation_view.add (new DeckView (model));
        }

        private void update_stack () {
            if (model.selected_deck != null) {
                navigation_view.push_by_tag ("deck");
            } else {
                navigation_view.pop_to_tag ("main");
            }
        }

        private void on_popped (Adw.NavigationPage page) {
            model.deck_edit_mode_active = false;
            model.selected_deck = null;
        }

        private void on_destroy () {
            model.deck_edit_mode_active = false;
            save_deck_data (model.data);
        }
    }
}


