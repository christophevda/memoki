/* model.vala
 *
 * Copyright 2022 Christophe Van den Abbeele
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Memoki {
    public class Shortcut : Object {
        public string[] accelerators { get; set; }
        public string description { get; set; }

        public Shortcut (string[] accelerators = {}, string description = "") {
            Object ();

            this.accelerators = accelerators;
            this.description = description;
        }

        public bool is_empty () {
            return description == "" && accelerators.length == 0;
        }
    }

    public class ShortcutDeck : Object, Json.Serializable {

        public string name { get; set; }
        public ListStore shortcuts { get; set; }

        public ShortcutDeck (string name = "") {
            Object ();

            this.name = name;
            shortcuts = new ListStore (typeof (Shortcut));
        }

        public virtual Json.Node serialize_property (string property_name, Value @value, ParamSpec pspec) {
            if (property_name == "shortcuts") {
                unowned ListStore shortcuts = @value as ListStore;
                var array = new Json.Array.sized (shortcuts.get_n_items ());

                for (var i = 0; i < shortcuts.get_n_items (); i++) {
                    var shortcut = (Shortcut) shortcuts.get_item (i);
                    array.add_element (Json.gobject_serialize (shortcut));
                }
                var node = new Json.Node (Json.NodeType.ARRAY);
                node.set_array (array);
                return node;
            }

            return default_serialize_property (property_name, @value, pspec);
        }

        public virtual bool deserialize_property (string property_name, out Value @value, ParamSpec pspec, Json.Node property_node) {
            if (property_name == "shortcuts") {
                var array = property_node.get_array ();
                var shortcuts = new ListStore (typeof (Shortcut));

                for (var i = 0; i < array.get_length (); i++) {
                    shortcuts.append (Json.gobject_deserialize (typeof (Shortcut), array.get_element (i)));
                }
                @value = shortcuts;
                return true;
            }

            return default_deserialize_property (property_name, out @value, pspec, property_node);
        }
    }

    public class ShortcutDeckCollection : Object, Json.Serializable {
        public ListStore decks { get; set; }


        public ShortcutDeckCollection () {
            Object ();
        }

        construct {
            decks = new ListStore (typeof (ShortcutDeck));
        }

        public virtual Json.Node serialize_property (string property_name, Value @value, ParamSpec pspec) {
            if (property_name == "decks") {
                unowned ListStore decks = @value as ListStore;
                var array = new Json.Array.sized (decks.get_n_items ());

                for (var i = 0; i < decks.get_n_items (); i++) {
                    var deck = (ShortcutDeck) decks.get_item (i);
                    array.add_element (Json.gobject_serialize (deck));
                }
                var node = new Json.Node (Json.NodeType.ARRAY);
                node.set_array (array);
                return node;
            }

            return default_serialize_property (property_name, @value, pspec);
        }

        public virtual bool deserialize_property (string property_name, out Value @value, ParamSpec pspec, Json.Node property_node) {
            if (property_name == "decks") {
                var array = property_node.get_array ();
                var decks = new ListStore (typeof (ShortcutDeck));

                for (var i = 0; i < array.get_length (); i++) {
                    decks.append (Json.gobject_deserialize (typeof (ShortcutDeck), array.get_element (i)));
                }
                @value = decks;
                return true;
            }

            return default_deserialize_property (property_name, out @value, pspec, property_node);
        }
    }

    public class Model : Object {
        public ShortcutDeckCollection data { get; set; }

        public bool deck_edit_mode_active { get; set; }

        public ShortcutDeck? selected_deck { get; set; }

        public Model (ShortcutDeckCollection d) {
            data = d;
            selected_deck = null;
            deck_edit_mode_active = false;

            notify["deck-edit-mode-active"].connect (update_deck_edit_mode);
        }

        public void add_deck (string name) {
            return_if_fail (data != null);
            data.decks.append (new ShortcutDeck (name));
        }

        public void remove_deck (ShortcutDeck d) {
            uint position;
            if (data.decks.find (d, out position)) {
                data.decks.remove (position);
            }
        }

        public void remove_shortcut (Shortcut s) {
            if (selected_deck == null) return;

            uint position;
            if (selected_deck.shortcuts.find (s, out position)) {
                selected_deck.shortcuts.remove (position);
            }
        }

        private void update_deck_edit_mode () {
            if (selected_deck == null) return;

            var shortcuts = selected_deck.shortcuts;
            for (var i = 0; i < shortcuts.n_items; i++) {
                var s = shortcuts.get_item (i) as Shortcut;
                if (s.is_empty ()) {
                    shortcuts.remove (i);
                    i--;
                }
            }

            if (deck_edit_mode_active) {
                add_empty_last_shortcut (shortcuts);
            }
        }

        private void add_empty_last_shortcut (ListStore shortcuts) {
            // message ("add_empty_last_shortcut");
            var last_shortcut = new Shortcut ();
            shortcuts.append (last_shortcut);
            last_shortcut.notify.connect (on_last_shortcut_change);
        }

        private void on_last_shortcut_change () {
            // message ("on_last_shortcut_change");
            if (!deck_edit_mode_active) return;

            var shortcuts = selected_deck.shortcuts;
            var last_shortcut = shortcuts.get_item (shortcuts.n_items - 1)
                as Shortcut;

            if (!last_shortcut.is_empty ()) {
                add_empty_last_shortcut (shortcuts);
            }
        }
    }
}
