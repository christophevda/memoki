`deck_view.vala`

Code cleanup + define some rules for order of properties / methods / callbacks.

Make shortcutrows removable.

Implement training window.

Implement training logic (simple first, just go through whole deck).

Implement importing and exporting of decks.

Implement importing and exporting of full progress.

Set up translation + mark all translatable strings correctly
